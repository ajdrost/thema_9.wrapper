/**
 * This is the Main class, here is where the program starts.
 * It asks for user arguments, if the wrong arguments are given then a IllegalStateException will appear
 * Copyright (c) 2019
 * @author Anne Jan Drost
 * @version 1.2
 */

package Classifier;

import java.util.Arrays;

public final class RandomForest  {

    /**
     * The main method.
     * @param args the command line arguments.
     * @exception IllegalStateException ex, if the given arguments are wrong.
     */

    public static void main(final String[] args) {
        try {
            optionClass op = new optionClass(args);

            if (op.helpRequested()) {
                op.PrintHelp();
                return;
            }

            userInputOptions optionObject = new userInputOptions(op.getArff(), op.getModel(), op.getUnknown());
            new Wrapper(optionObject);

        } catch (IllegalStateException ex) {
            System.err.println("Wrong command line usage \"" + Arrays.toString(args) + "\"");
            System.err.println("this went wrong!!" + ex.getMessage());
            optionClass op = new optionClass(new String[]{});
            op.PrintHelp();
        }

    }
}
