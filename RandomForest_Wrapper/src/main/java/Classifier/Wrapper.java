/**
 * This is the Wrapper class, if the right arguments are given then this class will execute a weka constructed acton,
 * that will calculate and predict the incomplete instances for you without using the weka GUI
 * Copyright (c) 2019
 * @author Anne Jan Drost
 * @version 1.2
 * */


package Classifier;

import weka.core.Instances;
import weka.core.Instance;
import weka.classifiers.trees.RandomForest;
import weka.core.converters.ConverterUtils.DataSource;
import weka.core.converters.ArffSaver;

import java.io.File;
import java.io.IOException;


class Wrapper {

    Wrapper(userInputOptions options) {
        start(options);
    }

    /**
     * starts the Weka wrapper.
     * @param options the command line arguments.
     */
    private void start(userInputOptions options) {
        String ArffFile = options.getArff();
        String UnknownFile = options.getUnknown();

        try {
            Instances instances = loadArff(ArffFile);
            printInstances(instances);

            RandomForest RF = buildClassifier(instances);
            saveClassifier(RF, options);

            RandomForest fromFile = loadClassifier(options);
            Instances unknownInstances = loadArff(UnknownFile);

            System.out.println("\nunclassified unknownInstances =");
            printinfo(unknownInstances);

            Instances newInstances = classifyNewInstance(fromFile, unknownInstances);
            Save_output(newInstances);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    //RandomForest;
    // classifyNewInstance
    /**
     * this classifies the unknown instanced into predicted one's.
     * @param trees, this is the RandomForest classifier.
     * @param unknownInstances, this is the file with the instances that you dont know yet.
     * @return labeled instances.
     */
    // @throws Exception, if wrong file.

    private Instances classifyNewInstance(RandomForest trees, Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = trees.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        System.out.println("\nNew, labeled ="); // this one was added
        printinfo(labeled);
        return labeled;
    }

    // loadClassifier
    /**
     *loads the Classifier model.
     * @param options, the UserInputOptions.
     * @return the RandomForest model file.
     */
    private RandomForest loadClassifier(userInputOptions options) throws Exception {
        // deserialize model
        String modelFile = options.getModel();
        return (RandomForest) weka.core.SerializationHelper.read(modelFile);
    }

    // saveClassifier

    /**
     *after building the classifier it will then be saved.
     * @param RF, the RandomForest model.
     * @param options, the UserInputOptions.
     */
    private void saveClassifier(RandomForest RF, userInputOptions options) throws Exception {
        String modelFile = options.getModel();
        //post 3.5.5
        // serialize model
        weka.core.SerializationHelper.write(modelFile, RF);
    }

    // buildClassifier

    /**
     * builds the classifier .
     * @param instances, the already known instances.
     * @return the RandomForest classifier.
     */
    private RandomForest buildClassifier(Instances instances) throws Exception {
        RandomForest RF = new RandomForest();
        RF.buildClassifier(instances);
        return RF;
    }

    // SAVE
    /**
     * saves the output to a designated folder.
     * @param instances, the newly made instances.
     */
    private void Save_output(Instances instances) throws IOException {
        ArffSaver save = new ArffSaver();
        save.setInstances(instances);
        save.setFile(new File("Output/output.txt"));
        System.out.println("\nFile has been saved to:\n Output/output.txt ");
        save.writeBatch();
    }

    // LOAD
    /**
     * fetches the unknown instances file.
     * @param datafile, the unknown file.
     * @return, returns the unknown instances file.
     */
    private Instances loadArff(String datafile) throws IOException {
        try {
            DataSource source = new DataSource(datafile);
            Instances data = source.getDataSet();

            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);
            return data;
        } catch (Exception e) {
            throw new IOException("could not read from file");
        }
    }

    // Print many
    /**
     * prints all the attributes and five attributes.
     * @param instances, a file of instances.
     */
    private void printInstances(Instances instances) {
        int numAttributes = instances.numAttributes();
        System.out.println("Known Instances = ");

        for (int i = 0; i < numAttributes; i++) {
            System.out.println("attribute " + i + " = " + instances.attribute(i));
        }

        System.out.println("\nclass index = " + instances.classIndex() + "\n");
        //or
        int numInstances = instances.numInstances();
        for (int i = 0; i < numInstances; i++) {
            if (i == 5) break;
            Instance instance = instances.instance(i);
            System.out.println("instance = " + instance);
        }
    }

    // Print less
    /**
     * this is almost the same as 'printInstances' but only prints the attributes.
     * @param instances, a file of instances.
     */
    private void printinfo(Instances instances) {
        //System.out.println();

        int numInstances = instances.numInstances();
        for (int i = 0; i < numInstances; i++) {
            if (i == 5) break;
            Instance instance = instances.instance(i);
            System.out.println("instance = " + instance);
        }

    }

}

