/**
 * This is the Options_Class and implements the Options_Prov interface,
 * this class will process the commandline for you.
 * Copyright (c) 2019
 * @author Anne Jan Drost
 * @version 1.2
 * */

package Classifier;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;


public class optionClass implements optionsProv {

    private static final String HELP = "help";
    private static final String ARFF = "arff";
    private static final String MODEL = "model";
    private static final String UNKNOWN = "unknown";

    private Options options;
    private String arff;
    private String model;
    private String unknown;
    private CommandLine commandLine;
    private final String[] clArguments;

    /**
     * starts initialize() with the arguments
     * @param args the command line arguments.
     */
    optionClass(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    /**
     * initializes buildOptions and processCommandLine
     */
    private void initialize() {
        buildOptions();
        processCommandLine();
    }

    /**
     * check if help was requested; if so, return true.
     * @return helpRequested
     */
    boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }

    /**
     * options builder for the user.
     */
    private void buildOptions() {
        // create Options object
        this.options = new Options();
        Option HelpOption = new Option("h", HELP, false, "Prints this message");
        Option ArffOption = new Option("a", ARFF, true, "Arff file for Weka");
        Option ModelOption = new Option("m", MODEL, true, "Model for Weka");
        Option UnknownOption = new Option("u", UNKNOWN, true, "the unknown instances, you want to analyse");

        // -a Files/white.wine.arff -m Files/RF.model -u Files/unknown_wine.arff

        options.addOption(HelpOption);
        options.addOption(ArffOption);
        options.addOption(ModelOption);
        options.addOption(UnknownOption);
    }

    /**
     * processes the command line arguments.
     */
    private void processCommandLine() {
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);

            //parse the args

            // Option arff
            if (this.commandLine.hasOption(ARFF)) {
                this.arff = commandLine.getOptionValue(ARFF).trim();
            } else{
                this.arff = "Files/white.wine.arff";
                System.out.println("no file was given, using default unknown instances\n");
                //throw new FileNotFoundException("File path needs to be defined");
            }

            // Option MODEL
            if (this.commandLine.hasOption(MODEL)){
                this.model = commandLine.getOptionValue(MODEL).trim();
            } else{
                this.model = "Files/RF.model";
                System.out.println("no model was given, using default Model\n");
                //throw new FileNotFoundException("Model needs to be defined, using default model");
            }

            // Option UNKNOWN
            if (this.commandLine.hasOption(UNKNOWN)){
                this.unknown = commandLine.getOptionValue(UNKNOWN).trim();
            } else{
                this.unknown = "Files/unknown_wine.arff";
                System.out.println("no file was given, using default instances\n");
                //throw new FileNotFoundException("Output path needs to be defined");
            }

            // CATCH
        } catch (ParseException ex) {
            System.err.println("Wrong arguments were given!! \"" + ex + "\"");
        }
        catch (IllegalArgumentException i){
            System.err.println("argument is not legal: \"" + i + "\"");
        }
        //catch (FileNotFoundException e) {
        //   System.err.println("missing files, please make sure you have the right path/file: \"" + e + "\"");
        //}
    }

    /**
     * if the user is confused and the help is needed.
     */
    void PrintHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("MyCoolTool", options);
    }

    /**
     *
     * @return the known Arff file.
     */
    @Override
    public String getArff() {
        return this.arff;
    }

    /**
     *
     * @return the model.
     */
    @Override
    public String getModel() {
        return this.model;
    }

    /**
     *
     * @return the unknown file.
     */
    @Override
    public String getUnknown() {
        return this.unknown;
    }
}
