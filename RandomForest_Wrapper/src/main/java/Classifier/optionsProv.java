/**
 * This is the Options interface and is implemented by OptionsClass and UserInputOptions.
 * Copyright (c) 2019
 * @author Anne Jan Drost
 * @version 1.2
 * */

package Classifier;

public interface optionsProv {

    /**
     * serves the Arff file
     * @return ARFF
     */
    String getArff();

    /**
     * serves the Model
     * @return MODEL
     */
    String getModel();

    /**
     * serves as the Unknown file
     * @return OUT
     */
    String getUnknown();

}