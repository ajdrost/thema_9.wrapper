/**
 * Copyright (c) 2019
 * @author Anne Jan Drost
 * @version 1.2
 * */


package Classifier;


public class userInputOptions implements optionsProv {

    private String ARFF;
    private String MODEL;
    private String UNKNOWN;

    /**
     * the user input options.
     * @param ARFF: the file containing the arff dataset.
     * @param MODEL: the users model.
     * @param UNKNOWN: the unknown instances.
     */
    userInputOptions(String ARFF, String MODEL, String UNKNOWN) {
        this.ARFF = ARFF;
        this.MODEL = MODEL;
        this.UNKNOWN = UNKNOWN;
    }

    /**
     * @return the ARFF.
     */
    @Override
    public String getArff() { return ARFF; }

    /**
     * @return the MODEL.
     */
    @Override
    public String getModel() { return MODEL; }

    /**
     * @return the UNKNOWN.
     */
    @Override
    public String getUnknown() { return UNKNOWN; }
}
